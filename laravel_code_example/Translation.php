<?php

namespace App\Models;

use XandaNet\MysqlEncrypt\Traits\Encryptable;
use App\Models\User;
use App\Models\Group;
use App\Models\Company;
use App\Models\Profile;
use App\Models\BaseModel;
use App\Models\SkillLevel;
use App\Models\ProfileSkill;
use App\Models\LanguageLevel;
use App\Models\ProfileProject;
use App\Models\ProfileVersion;
use App\Models\ProfileLanguage;
use App\Models\ProfileEmployment;
use App\Models\ProfileCertificate;
use Illuminate\Support\Facades\DB;

class Translation extends BaseModel
{
    use Encryptable;
    
    protected $fillable = ['language', 'table_name', 'object_id', 'field_name', 'value'];
    protected $encryptable = ['language', 'table_name', 'object_id', 'field_name', 'value'];

    const OBJECT_TABLES = [
        'profiles'                  =>  Profile::class,
        'language_levels'           =>  LanguageLevel::class,
        'skill_levels'              =>  SkillLevel::class,
        'companies'                 =>  Company::class,
        'groups'                    =>  Group::class,
        'profile_certificates'      =>  ProfileCertificate::class,
        'profile_languages'         =>  ProfileLanguage::class,
        'profile_employments'       =>  ProfileEmployment::class,
        'profile_projects'          =>  ProfileProject::class,
        'profile_skills'            =>  ProfileSkill::class,
        'profile_versions'          =>  ProfileVersion::class,
        'user'                      =>  User::class,
    ];

    public function getObject()
    {
        $classes = self::OBJECT_TABLES;
        $modelName = $classes[$this->table_name];
        return $modelName::find($this->object_id);
    }
}
