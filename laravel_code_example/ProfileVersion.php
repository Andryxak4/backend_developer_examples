<?php

namespace App\Models;

use XandaNet\MysqlEncrypt\Traits\Encryptable;

use App;

class ProfileVersion extends BaseTranslatableContentModel
{
    use Encryptable;

    protected $fillable = ['profile_id', 'career_start', 'career_break', 'education_from', 'education_to'];
    protected $encryptable = ['career_start', 'career_break', 'education_from', 'education_to'];
    public static $contentFields = ['name', 'position', 'phone', 'address_street', 'address_number', 'address_city',
        'address_postcode', 'address_country', 'address_not_living_there', 'real_address', 'email', 'gdpr', 'summary',
        'education_school_name', 'education_study_field', 'others_header', 'others_text'];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            foreach ($model->skills as $rel) {
                $rel->delete();
            }
            foreach ($model->languages as $rel) {
                $rel->delete();
            }
            foreach ($model->certificates as $rel) {
                $rel->delete();
            }
            foreach ($model->employments as $rel) {
                $rel->delete();
            }
        });
    }

    public static function cloneOf($model, $attributes=[])
    {
        $clone = parent::cloneOf($model, $attributes);
        foreach ($model->skills as $rel) {
            $newRel = ProfileSkill::cloneOf($rel, ['profile_version_id' => $clone->id]);
        }
        foreach ($model->languages as $rel) {
            $newRel = ProfileLanguage::cloneOf($rel, ['profile_version_id' => $clone->id]);
        }
        foreach ($model->certificates as $rel) {
            $newRel = ProfileCertificate::cloneOf($rel, ['profile_version_id' => $clone->id]);
        }
        foreach ($model->employments as $rel) {
            $newRel = ProfileEmployment::cloneOf($rel, ['profile_version_id' => $clone->id]);
        }
        return $clone;
    }

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    public function skills()
    {
        return $this->hasMany(ProfileSkill::class)->orderBy('position');
    }

    public function languages()
    {
        return $this->hasMany(ProfileLanguage::class)->orderBy('position');
    }

    public function certificates()
    {
        return $this->hasMany(ProfileCertificate::class)->orderBy('position');
    }

    public function employments()
    {
        return $this->hasMany(ProfileEmployment::class)->orderBy('position');
    }

    public function journalWriter()
    {
        return $this->hasMany(JournalWriter::class);
    }

    public function getKeySkillAttribute()
    {
        $skill_names = [];
        foreach ($this->skills as $skill):
            if ($skill->name):
                $skill_names[] = $skill->name;
            endif;
        endforeach;
        return implode(', ', $skill_names);
    }

    public function getAssignedToAttribute()
    {
        return __('No assignment');
    }

    public function getLastActionAttribute()
    {
        return ($this->created_at == $this->updated_at) ? __('Profile created') : __('Profile updated');
    }

    public function getLastActionUserAttribute()
    {
        return data_get($this->profile, 'userCreated.name');
    }

    public function getCareerExperienceAttribute()
    {
        if (!$this->career_start) return 0;
        return date('Y') - $this->career_start - $this->career_break + 1;
    }

    public function getHasSkillsAttribute()
    {
        foreach ($this->skills as $model) {
            if ($model->name) {
                return true;
            }
        }
        return false;
    }

    public function getHasCertificatesAttribute()
    {
        foreach ($this->certificates as $model) {
            if ($model->name) {
                return true;
            }
        }
        return false;
    }

    public function getHasLanguagesAttribute()
    {
        foreach ($this->languages as $model) {
            if ($model->name) {
                return true;
            }
        }
        return false;
    }

    public function getHasEducationAttribute()
    {
        return $this->education_from || $this->education_to || $this->education_school_name || $this->education_study_field;
    }

    public function getFormVars()
    {
        $all_groups = Group::all();
        $groups = [];
        foreach ($all_groups as $group):
            $groups[$group->id] = $group->name;
        endforeach;
        $all_companies = Company::all();
        $companies = [];
        foreach ($all_companies as $model):
            $companies[$model->id] = $model->name;
        endforeach;
        $last50years = [];
        for ($year = 0; $year < 50; $year++):
            $last50years[date('Y')-$year] = date('Y')-$year;
        endfor;
        $all_skills = ProfileSkill::select('name')->distinct()->orderBy('name')->get();
        $skill_names = [];
        foreach ($all_skills as $skill):
            $skill_names[$skill->name] = $skill->name;
        endforeach;
        $all_skill_levels = Translation::select(['object_id', 'value'])
                ->where([
                    ['language', db_encrypt(App::getLocale())],
                    ['table_name', db_encrypt(app(SkillLevel::class)->getTable())],
                    ['field_name', db_encrypt('name')]
                ])
                ->distinct()->orderBy('value')->get();
        $skill_levels = [];
        foreach ($all_skill_levels as $level):
            $skill_levels[$level->object_id] = $level->value;
        endforeach;
        $all_languages = Translation::select('value')
                ->where([
                    ['language', db_encrypt(App::getLocale())],
                    ['table_name', db_encrypt(app(ProfileLanguage::class)->getTable())],
                    ['field_name', db_encrypt('name')]
                ])
                ->distinct()->orderBy('value')->get();
        $language_names = [];
        foreach ($all_languages as $language):
            $language_names[$language->value] = $language->value;
        endforeach;
        $all_language_levels = Translation::select(['object_id', 'value'])
                ->where([
                    ['language', db_encrypt(App::getLocale())],
                    ['table_name', db_encrypt(app(LanguageLevel::class)->getTable())],
                    ['field_name', db_encrypt('name')]
                ])
                ->distinct()->orderBy('value')->get();
        $language_levels = [];
        foreach ($all_language_levels as $level):
            $language_levels[$level->object_id] = $level->value;
        endforeach;
        return [
            'profileVersion' => $this,
            'groups' => $groups,
            'companies' => $companies,
            'last50years' => $last50years,
            'skill_names' => $skill_names,
            'skill_levels' => $skill_levels,
            'language_names' => $language_names,
            'language_levels' => $language_levels,
        ];
    }

    public function getSkillsRowsAttribute()
    {
        $result = [];
        foreach ($this->skills as $row):
            $result[$row->position] = [
                'id' => $row->id,
                'name' => $row->name,
                'level' => data_get($row, 'skillLevel.id'),
            ];
        endforeach;
        return $result;
    }

    public function getLanguagesRowsAttribute()
    {
        $result = [];
        foreach ($this->languages as $row):
            $result[$row->position] = [
                'id' => $row->id,
                'name' => $row->name,
                'level' => data_get($row, 'languageLevel.id'),
            ];
        endforeach;
        return $result;
    }

    public function getCertificatesRowsAttribute()
    {
        $result = [];
        foreach ($this->certificates as $row):
            $result[$row->position] = [
                'id' => $row->id,
                'type' => $row->type,
                'name' => $row->name,
                'year' => $row->year,
                'valid' => $row->valid_up_to,
            ];
        endforeach;
        return $result;
    }

    public function getEmploymentsRowsAttribute()
    {
        $result = [];
        foreach ($this->employments as $row):
            $projects = [];
            foreach ($row->projects as $project):
                $lines = [];
                foreach (['description', 'role', 'duration', 'technologies', 'customer', 'tasks'] as $field):
                    if ($field == 'duration'):
                        if ($project->from):
                            $lines[$field] = [
                                'pfield' => $field,
                                'pfrom' => $project->from,
                                'pto' => $project->to,
                            ];
                        endif;
                    else:
                        if ($project->$field):
                            $value_key = ($field == 'tasks') ? 'ptasks' : 'pvalue';
                            $lines[$field] = [
                                'pfield' => $field,
                                $value_key => $project->$field
                            ];
                        endif;
                    endif;
                endforeach;
                $projects[$project->position] = [
                    'pid' => $project->id,
                    'pname' => $project->name,
                    '_child' => [
                        'class' => 'project-details',
                        'rows' => $lines,
                    ],
                ];
            endforeach;
            $result[$row->position] = [
                'id' => $row->id,
                'employer' => $row->employer,
                'role' => $row->role,
                'from' => $row->from,
                'to' => $row->to,
                'task' => $row->main_task,
                '_child' => [
                    'class' => 'employer-projects',
                    'rows' => $projects,
                ],
            ];
        endforeach;
        return $result;
    }

    public function saveSkills($skills_data)
    {
        $old_models = [];
        foreach ($this->skills as $model) {
            $old_models[$model->id] = $model;
        }
        foreach ($skills_data as $i => $item) {
            if (!empty($item['id']) && isset($old_models[$item['id']])) {
                $model = $old_models[$item['id']];
                unset($old_models[$item['id']]);
            } else {
                $model = new ProfileSkill([
                    'profile_version_id' => $this->id,
                ]);
            }
            if (!empty($item['name'])) {
                $level_id = $item['level'];
                $model->name = $item['name'];
                $model->skill_level_id = $level_id;
                $model->position = $i;
                $model->save();
            }
        }

        foreach ($old_models as $model) {
            $model->delete();
        }
    }

    public function saveCertificates($certificates_data)
    {
        $old_models = [];
        foreach ($this->certificates as $model) {
            $old_models[$model->id] = $model;
        }
        foreach ($certificates_data as $i => $item) {
            if (!empty($item['id']) && isset($old_models[$item['id']])) {
                $model = $old_models[$item['id']];
                unset($old_models[$item['id']]);
            } else {
                $model = new ProfileCertificate([
                    'profile_version_id' => $this->id,
                ]);
            }
            if (empty($item['type'])) continue;
            $model->type = $item['type'];
            $model->year = $item['year'];
            $model->valid_up_to = $item['type'] == ProfileCertificate::TYPE_CERTIFICATE ? $item['valid'] : null;
            $model->position = $i;
            $model->save();
            $model->saveContent([
                'name' => $item['name'],
            ]);
        }

        foreach ($old_models as $model) {
            $model->delete();
        }
    }

    public function saveLanguages($languages_data)
    {
        $old_models = [];
        foreach ($this->languages as $model) {
            $old_models[$model->id] = $model;
        }
        foreach ($languages_data as $i => $item) {
            if (!empty($item['id']) && isset($old_models[$item['id']])) {
                $model = $old_models[$item['id']];
                unset($old_models[$item['id']]);
            } else {
                $model = new ProfileLanguage([
                    'profile_version_id' => $this->id,
                ]);
            }
            if (!empty($item['name'])) {
                $level_id = $item['level'];
                $model->language_level_id = $level_id;
                $model->position = $i;
                $model->save();
                $model->saveContent([
                    'name' => $item['name'],
                ]);
            }
        }

        foreach ($old_models as $model) {
            $model->delete();
        }
    }

    public function saveEmployments($employment_data)
    {
        $old_models = [];
        foreach ($this->employments as $model) {
            $old_models[$model->id] = $model;
        }
        foreach ($employment_data as $i => $item) {
            if (!empty($item['id']) && isset($old_models[$item['id']])) {
                $model = $old_models[$item['id']];
                unset($old_models[$item['id']]);
            } else {
                $model = new ProfileEmployment([
                    'profile_version_id' => $this->id,
                ]);
            }
            $model->from = $item['from'];
            $model->to = $item['to'];
            $model->position = $i;
            $model->save();
            $model->saveContent([
                'employer' => $item['employer'],
                'role' => $item['role'],
                'main_task' => $item['task'],
            ]);
            $projects_data = data_get($item, 'project', []);
            $model->saveProjects($projects_data);
        }

        foreach ($old_models as $model) {
            $model->delete();
        }
    }
}
