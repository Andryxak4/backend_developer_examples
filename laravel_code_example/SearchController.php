<?php

namespace App\Http\Controllers;

use App;
use App\Models\Profile;
use App\Models\ProfileVersion;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SearchController extends BaseDashboardController
{
    public static function accessRules()
    {
        return array_merge(parent::accessRules(), [
            'index' => [
                'label' => __('Search page'),
                'permission' => 'search.index'
            ],
        ]);
    }

    public function index()
    {
        $profiles = [];
        return view('search/index', [
            'profiles' => $profiles,
        ]);
    }

    public function ajax(Request $request)
    {
        $request->validate([
            'query' => 'required',
            'type' => ['required', Rule::in(['profile', 'person', 'skill', 'full'])],
            'mode' => ['required', Rule::in(['or', 'and'])],
        ]);
        $textToSearch = $request->input('query', '');
        $type = $request->input('type');
        $mode = $request->input('mode');
        $key = config('mysql-encrypt.key');
        $profileVersions = [];
        if ($type == 'profile'):
            $idsFound = DB::table('translations')
                    ->selectRaw('AES_DECRYPT(`object_id`, ?) as objectId', [$key])
                    ->whereRaw('AES_DECRYPT(`table_name`, ?) = "profile_versions"'
                        . ' AND AES_DECRYPT(`field_name`, ?) = "name"'
                        . ' AND CONVERT(AES_DECRYPT(`value`, ?) USING "utf8") LIKE ?'
                        . ' AND AES_DECRYPT(`language`, ?) = ?',
                            [$key, $key, $key, '%'.$textToSearch.'%', $key, App::getLocale()])
                    ->pluck('objectId');

            $profileVersions = ProfileVersion::with('profile')
                    ->whereIn('id', $idsFound)
                    ->orderBy('profile_id', 'asc')
                    ->get();

        elseif ($type == 'person'):
            $idsFound = DB::table('translations')
                    ->selectRaw('AES_DECRYPT(`object_id`, ?) as objectId', [$key])
                    ->whereRaw('AES_DECRYPT(`table_name`, ?) = "profiles"'
                        . ' AND AES_DECRYPT(`field_name`, ?) IN ("name", "surname")'
                        . ' AND CONVERT(AES_DECRYPT(`value`, ?) USING "utf8") LIKE ?'
                        . ' AND AES_DECRYPT(`language`, ?) = ?',
                            [$key, $key, $key, '%'.$textToSearch.'%', $key, App::getLocale()])
                    ->pluck('objectId');

            $profileVersions = ProfileVersion::with('profile')
                    ->whereIn('profile_id', $idsFound)
                    ->orderBy('profile_id', 'asc')
                    ->get();

        elseif ($type == 'skill'):
            $textToSearch = preg_split('/[,\s]+/', $textToSearch);

            $profileVersionsQuery = ProfileVersion::with(['profile', 'skills']);
            if ($mode == 'or'):
                $profileVersionsQuery->whereRaw('0=1');
            endif;
            foreach ($textToSearch as $textFragment):
                $profileVersionsQuery->has('skills', '>=', 1, $mode, function($query) use ($textFragment, $key) {
                    $query->whereRaw('CONVERT(AES_DECRYPT(`name`, ?) USING "utf8") LIKE ?', [$key, '%'.$textFragment.'%']);
                });
            endforeach;
            $profileVersions = $profileVersionsQuery
                    ->orderBy('profile_id', 'asc')
                    ->get();

        elseif ($type == 'full'):
            $textToSearch = preg_split('/[, ]+/', $textToSearch);
            $versionsFound = [];
            foreach ($textToSearch as $fragmentIndex => $textFragment):
                $rowsFound = DB::table('translations')
                        ->selectRaw('AES_DECRYPT(`object_id`, ?) as objectId, AES_DECRYPT(`table_name`, ?) as tableName', [$key, $key])
                        ->whereRaw('AES_DECRYPT(`language`, ?) = ?', [$key, App::getLocale()])
                        ->where(function($query) use ($textFragment, $key) {
                            $query->whereRaw('CONVERT(AES_DECRYPT(`value`, ?) USING "utf8") LIKE ?', [$key, '%'.$textFragment.'%']);
                        })
                        ->havingRaw("tableName IN ('profiles','profile_certificates','profile_employments','profile_languages',"
                                . "'profile_projects','profile_skills','profile_versions','skill_levels','language_levels','companies','groups')")
                        ->groupByRaw('tableName, objectId')
                        ->get();
                $idsByRelation = [];
                foreach ($rowsFound as $row):
                    $idsByRelation[$row->tableName][] = $row->objectId;
                endforeach;

                $profileVersionsQuery = ProfileVersion::with([
                            'profile',
                            'profile.company',
                            'profile.group',
                            'certificates',
                            'employments',
                            'languages',
                            'employments.projects',
                            'skills',
                            'skills.skillLevel'
                        ])
                        ->whereRaw('0=1');
                if (!empty($idsByRelation['profiles'])):
                    $profileVersionsQuery->orWhereIn('profile_id', $idsByRelation['profiles']);
                endif;
                $profileVersionsQuery->orWhereRaw('CONVERT(AES_DECRYPT(`career_start`, ?) USING "utf8") LIKE ?', [$key, '%'.$textFragment.'%']);
                $profileVersionsQuery->orWhereRaw('CONVERT(AES_DECRYPT(`career_break`, ?) USING "utf8") LIKE ?', [$key, '%'.$textFragment.'%']);
                $profileVersionsQuery->orWhereRaw('CONVERT(AES_DECRYPT(`education_from`, ?) USING "utf8") LIKE ?', [$key, '%'.$textFragment.'%']);
                $profileVersionsQuery->orWhereRaw('CONVERT(AES_DECRYPT(`education_to`, ?) USING "utf8") LIKE ?', [$key, '%'.$textFragment.'%']);
                if (!empty($idsByRelation['profile_versions'])):
                    $profileVersionsQuery->orWhereIn('id', $idsByRelation['profile_versions']);
                endif;
                $profileVersionsQuery->orWhereHas('skills', function($query) use ($textFragment, $key, $idsByRelation) {
                    $query->whereRaw('CONVERT(AES_DECRYPT(`name`, ?) USING "utf8") LIKE ?', [$key, '%'.$textFragment.'%']);
                    if (!empty($idsByRelation['profile_skills'])):
                        $query->orWhereIn('id', $idsByRelation['profile_skills']);
                    endif;
                });
                $profileVersionsQuery->orWhereHas('certificates', function($query) use ($textFragment, $key, $idsByRelation) {
                    $query->whereRaw('0=1');
                    $query->orWhereRaw('CONVERT(AES_DECRYPT(`year`, ?) USING "utf8") LIKE ?', [$key, '%'.$textFragment.'%']);
                    $query->orWhereRaw('CONVERT(AES_DECRYPT(`valid_up_to`, ?) USING "utf8") LIKE ?', [$key, '%'.$textFragment.'%']);
                    if (!empty($idsByRelation['profile_certificates'])):
                        $query->orWhereIn('id', $idsByRelation['profile_certificates']);
                    endif;
                });
                $profileVersionsQuery->orWhereHas('employments', function($query) use ($textFragment, $key, $idsByRelation) {
                    $query->whereRaw('0=1');
                    $query->orWhereRaw('CONVERT(AES_DECRYPT(`from`, ?) USING "utf8") LIKE ?', [$key, '%'.$textFragment.'%']);
                    $query->orWhereRaw('CONVERT(AES_DECRYPT(`to`, ?) USING "utf8") LIKE ?', [$key, '%'.$textFragment.'%']);
                    if (!empty($idsByRelation['profile_employments'])):
                        $query->orWhereIn('id', $idsByRelation['profile_employments']);
                    endif;
                });
                $profileVersionsQuery->orWhereHas('employments.projects', function($query) use ($textFragment, $key, $idsByRelation) {
                    $query->whereRaw('0=1');
                    $query->orWhereRaw('CONVERT(AES_DECRYPT(`from`, ?) USING "utf8") LIKE ?', [$key, '%'.$textFragment.'%']);
                    $query->orWhereRaw('CONVERT(AES_DECRYPT(`to`, ?) USING "utf8") LIKE ?', [$key, '%'.$textFragment.'%']);
                    if (!empty($idsByRelation['profile_projects'])):
                        $query->orWhereIn('id', $idsByRelation['profile_projects']);
                    endif;
                });
                $relationByTable = [
                    'companies' => 'profile.company',
                    'groups' => 'profile.group',
                    'profile_languages' => 'languages',
                    'skill_levels' => 'skills.skillLevel',
                    'language_levels' => 'languages.languageLevel'
                ];
                foreach ($relationByTable as $tableName => $relationName):
                    if (!empty($idsByRelation[$tableName])):
                        $profileVersionsQuery->orWhereHas($relationName, function($query) use ($idsByRelation, $tableName) {
                            $query->whereIn('id', $idsByRelation[$tableName]);
                        });
                    endif;
                endforeach;

                $versionsFragment = $profileVersionsQuery
                        ->orderBy('profile_id', 'asc')
                        ->get();
                foreach ($versionsFragment as $version):
                    $versionsFound[$fragmentIndex]['id'.$version->id] = $version;
                endforeach;
            endforeach;

            if ($mode == 'or'):
                $profileVersions = call_user_func_array('array_merge', $versionsFound);
            else:
                $profileVersions = call_user_func_array('array_intersect_key', $versionsFound);
            endif;
        endif;

        $result = [];
        foreach ($profileVersions as $profileVersion):
            $item = [];
            if (Auth::user()->canany(['profiles.view', 'profiles.pdf'])):
                $view_col = '';
                if (Auth::user()->can('profiles.view', $profileVersion)):
                    $view_col .= '<a href="' . route('profiles.view', [App::getLocale(), 'id' => $profileVersion->id]) . '"'
                        . ' class="btn" title="' . __('View') . '"><i class="fa fa-eye"></i></a>';
                endif;
                if (Auth::user()->can('profiles.pdf', $profileVersion)):
                    $view_col .= '<a href="' . route('profiles.pdf', [App::getLocale(), 'id' => $profileVersion->id]) . '"'
                        . ' class="btn" title="' . __('PDF') . '"><i class="fa fa-file-pdf-o"></i></a>';
                endif;
                $item[] = $view_col;
            endif;
            if (Auth::user()->can('profiles.edit', $profileVersion)):
                $item[] = '<a href="' . route('profiles.edit', [App::getLocale(), 'id' => $profileVersion->id]) . '"'
                    . ' class="btn" title="' . __('Edit') . '"><i class="fa fa-edit"></i></a>';
            endif;
            $item = array_merge($item, [
                $profileVersion->profile->id,
                $profileVersion->name,
                $profileVersion->profile->name,
                $profileVersion->profile->surname,
                $profileVersion->position,
                data_get($profileVersion->profile, 'company.name'),
                $profileVersion->key_skill,
                data_get($profileVersion->profile, 'group.name'),
                $profileVersion->assigned_to,
                $profileVersion->last_action,
                $profileVersion->last_action_user,
                $profileVersion->updated_at->format('Y-m-d H:i:s')
            ]);
            $result[] = $item;
        endforeach;
        return [
            'success' => true,
            'results' => $result,
        ];
    }
}
