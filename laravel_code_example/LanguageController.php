<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\Translation;
use App\Models\LanguageFragment;
use App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;


class LanguageController extends BaseDashboardController
{
    public static function accessRules()
    {
        return array_merge(parent::accessRules(), [
            'index' => [
                'label' => __('Translations page'),
                'permission' => 'language.index'
            ],
            'indexPdf' => [
                'label' => __('PDF settings page'),
                'permission' => 'language.indexPdf'
            ],
        ]);
    }

    public function index()
    {
        $fragmentGroup = [];

        foreach (LanguageFragment::all() as $key => $value) {
            $fragmentGroup[$value->id] = json_decode($value->getAttributes()['text']); //ToDo make
        }

        return view('language', [
            'pageTitle' => __('messages.translations'),
            'fragmentGroup' => $fragmentGroup,
        ]);
    }

    public function indexPdf()
    {
        $fragmentGroup = [];

        foreach (LanguageFragment::where('key', 'LIKE', '%' . 'settings.pdf' . '%')->get() as $key => $value) {
            $fragmentGroup[$value->id] = json_decode($value->getAttributes()['text']); //ToDo make
        }

        return view('language', [
            'pageTitle' => __('messages.PDF settings'),
            'fragmentGroup' => $fragmentGroup,
        ]);
    }

    public function putLanguage(Request $req, $locale, $id)
    {
        $field = LanguageFragment::findOrfail($id);
        $requestField = $req->input('languageValue');
        if ($requestField != null) {
           $textFromFragment = $field->old_translation;
           $json = json_decode($textFromFragment, true);
           $json[$locale] = $requestField;
            $textResult = json_encode($json);
            $field->fill(['old_translation' => $textResult]);
        } else {
            $json[$locale] = " ";
            $textResult = json_encode($json);
            $field->fill(['old_translation' => $textResult]);
        }
        $field->save();

        DB::table('language_fragments')->where('id', $id)->update(array(
            'text' => $field->old_translation,
        ));

        return 'Update static language text';
    }

    public function insertLanguage(Request $req)
    {
        $text = ($req->input('locales'));

        $field = new LanguageFragment();
        $field->fill([
            'text' => json_decode($req->input('locales')),
            'key' => $req->input('key')
        ]);
        $field->save();
        return 'Update static language text';

    }
}
