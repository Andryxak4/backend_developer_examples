<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Profile;
use App\Models\Group;
use App\Models\ProfileVersion;
use App\Http\Requests\StoreProfileRequest;
use App\Http\Resources\ProfilesVersionResource;

class ProfilesController extends BaseDashboardController
{
    public static function accessRules()
    {
        return array_merge(parent::accessRules(), [
            'index' => [
                'label' => __(':model list', ['model' => __('Profiles')]),
                'permission' => 'profiles.index'
            ],
            'create' => [
                'label' => __('Create :model', ['model' => __('Profile')]),
                'permission' => 'profiles.create'
            ],
            'store' => [
                'label' => __('Create :model', ['model' => __('Profile')]),
                'permission' => 'profiles.create'
            ],
            'view' => [
                'label' => __('View :model', ['model' => __('Profile')]),
                'permission' => 'profiles.view'
            ],
            'edit' => [
                'label' => __('Edit :model', ['model' => __('Profile')]),
                'permission' => 'profiles.edit'
            ],
            'update' => [
                'label' => __('Edit :model', ['model' => __('Profile')]),
                'permission' => 'profiles.edit'
            ],
            'destroy' => [
                'label' => __('Delete :model', ['model' => __('Profile')]),
                'permission' => 'profiles.destroy'
            ],
            'pdf' => [
                'label' => __('Save :model as PDF', ['model' => __('Profile')]),
                'permission' => 'profiles.pdf'
            ],
            'cloneVersion' => [
                'label' => __('Clone :model', ['model' => __('Profile')]),
                'permission' => 'profiles.cloneVersion'
            ],
        ]);
    }

   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::all();
        $profiles = Profile::all();
        $versions = ProfileVersion::all();
        $profilesByGroup = [];
        foreach ($profiles as $profile):
            $profilesByGroup[$profile->group_id][$profile->id] = [
                'name' => $profile->name,
                'surname' => $profile->surname,
            ];
        endforeach;
        $versionsByProfile = [];
        foreach ($versions as $version):
            $versionsByProfile[$version->profile_id][$version->id] = $version->name;
        endforeach;
        return view('profiles.index', [
            'groups' => $groups,
            'profiles' => $profilesByGroup,
            'versions' => $versionsByProfile,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $version = new ProfileVersion();
        return view('profiles.create', $version->getFormVars());
    }

    public function store(StoreProfileRequest $request)
    {
        $versionId = null;
        DB::transaction(function () use ($request, &$versionId) {
            $id = $request->input('id');
            $content_language = $request->input('content_language', App::getLocale());
            App::setLocale($content_language);
            $group_id = $request->input('group_id');
            $company_id = $request->input('company_id');
            $skills_data = $request->input('skill', []);
            $certificates_data = $request->input('certificate', []);
            $languages_data = $request->input('language', []);
            $employment_data = $request->input('employment', []);

            $profile = new Profile([
                'id' => $id,
                'company_id' => $company_id,
                'group_id' => $group_id,
                'created_by' => Auth::user()->id,
            ]);
            $profile->save();
            $p_contentData = [];
            foreach (Profile::$contentFields as $field) {
                $p_contentData[$field] = $request->input($field);
            }
            $profile->saveContent($p_contentData);
            $profileVersion = new ProfileVersion([
                'profile_id' => $profile->id,
                'career_start' => $request->input('career_start'),
                'career_break' => $request->input('career_break'),
                'education_from' => $request->input('education_from'),
                'education_to' => $request->input('education_to'),
            ]);
            $profileVersion->save();
            $versionId = $profileVersion->id;
            $pv_contentData = [];
            foreach (ProfileVersion::$contentFields as $field) {
                $request_field = $field == 'name' ? 'profile_name' : $field;
                $pv_contentData[$field] = $request->input($request_field);
            }
            $profileVersion->saveContent($pv_contentData);

            $profileVersion->saveSkills($skills_data);
            $profileVersion->saveCertificates($certificates_data);
            $profileVersion->saveLanguages($languages_data);
            $profileVersion->saveEmployments($employment_data);
        });

        $isAjax = $request->input('ajax');
        if ($isAjax) {
            exit(json_encode([
                'success' => true,
                'url' => route('profiles.edit', ['locale' => App::getLocale(), 'id' => $versionId]),
                'formAction' => route('profiles.update', ['locale' => App::getLocale(), 'id' => $versionId])
            ]));
        }

        return redirect(route('home', App::getLocale()));
    }

    public function view($locale, $id)
    {
        return view('profiles.preview', [
            'pdfUrl' => route('profiles.pdf', [App::getLocale(), $id]),
        ]);
    }

    public function pdf($locale, $id)
    {
        $version = ProfileVersion::findOrFail($id);
        $pdf = new \Mpdf\Mpdf();
        $view = view('profiles.view', [
            'profileVersion' => $version,
        ]);
        $html = $view->render();
        $pdf->WriteHTML($html);
        return $pdf->Output();
    }

    public function edit($locale, $id)
    {
        $version = ProfileVersion::findOrFail($id);
        return view('profiles.create', $version->getFormVars());
    }

    public function update(Request $request, $locale, $id)
    {
        $version = ProfileVersion::findOrFail($id);
        DB::transaction(function () use ($request, $version) {
            $group_id = $request->input('group_id');
            $company_id = $request->input('company_id');
            $skills_data = $request->input('skill', []);
            $certificates_data = $request->input('certificate', []);
            $languages_data = $request->input('language', []);
            $employment_data = $request->input('employment', []);

            $version->profile->group_id = $group_id;
            $version->profile->company_id = $company_id;
            $version->profile->save();
            $p_contentData = [];
            foreach (Profile::$contentFields as $field) {
                $p_contentData[$field] = $request->input($field);
            }
            $version->profile->saveContent($p_contentData);

            $version->career_start = $request->input('career_start');
            $version->career_break = $request->input('career_break');
            $version->education_from = $request->input('education_from');
            $version->education_to = $request->input('education_to');
            $version->save();
            $pv_contentData = [];
            foreach (ProfileVersion::$contentFields as $field) {
                $request_field = $field == 'name' ? 'profile_name' : $field;
                $pv_contentData[$field] = $request->input($request_field);
            }
            $version->saveContent($pv_contentData);

            $version->saveSkills($skills_data);
            $version->saveCertificates($certificates_data);
            $version->saveLanguages($languages_data);
            $version->saveEmployments($employment_data);
        });

        $isAjax = $request->input('ajax');
        if ($isAjax) {
            $response = [
                'success' => true,
            ];
            $next = $request->input('next');
            if ($next == 'view' || $next == 'pdf'):
                $response['redirect'] = route('profiles.'.$next, ['locale' => App::getLocale(), 'id' => $id]);
            endif;
            exit(json_encode($response));
        }
        return redirect(route('home', App::getLocale()));
    }

    public function destroy($locale, $id)
    {
        $version = ProfileVersion::findOrFail($id);
        if (count($version->profile->versions) == 1) {
            $version->profile->delete();
        } else {
            $version->delete();
        }
        return json_encode(['success' => true]);
    }

    public function cloneVersion($locale, $id, Request $request)
    {
        $version = ProfileVersion::findOrFail($id);
        $name = $request->input('name');
        $profile_name_translation = App\Models\Translation::where([
            ['language', db_encrypt($locale)],
            ['table_name', db_encrypt($version->getTable())],
            ['field_name', db_encrypt('name')],
            ['value', db_encrypt($name)],
        ])->first();
        if ($profile_name_translation) {
            return json_encode([
                'success' => false,
                'sameName' => true,
                'message' => __('This name is already used'),
            ]);
        }
        $newVersion = ProfileVersion::cloneOf($version);
        $newVersion->saveContent([
            'name' => $name,
        ]);
        return json_encode([
            'success' => true,
            'id' => $newVersion->id,
        ]);
    }

    public function getProfileVersionsPaginate(Request $request) {
        return response()->json(['data' => ProfilesVersionResource::collection(ProfileVersion::select('id')->get())]);
    }
}
