<?php

namespace App\Models;

use XandaNet\MysqlEncrypt\Traits\Encryptable;
use Illuminate\Support\Facades\Route;

class Role extends BaseModel
{
    use Encryptable;

    protected $fillable = ['name', 'permissions'];
    protected $encryptable = ['name', 'permissions'];

    public static function allPermissions()
    {
        $allPermissions = ['*' => __('All')];
        foreach (Route::getRoutes()->getRoutes() as $route) {
            $action = $route->getAction();
            if (!array_key_exists('controller', $action)) continue;
            if (strpos($action['controller'], '@') === false) continue;
            list($controllerName, $actionName) = explode('@', $action['controller']);
            if (!method_exists($controllerName, 'accessRules')) continue;
            $rules = $controllerName::accessRules();
            if (!is_array($rules)) continue;
            foreach ($rules as $action => $rule_data):
                $allPermissions[$rule_data['permission']] = $rule_data['label'];
            endforeach;
        }
        return $allPermissions;
    }

    public function getPermissionsArrayAttribute()
    {
        $permissions = json_decode($this->permissions, true) ?: [];
        return $permissions;
    }
}
