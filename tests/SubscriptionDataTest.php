<?php

use FrontApi\Inc\Exceptions\FrontApiException;
use FrontApi\Inc\Exceptions\NewInstanceException;
use FrontApi\Inc\Models\Subscription;
use PHPUnit\Framework\TestCase;

/**
 * Class SubscriptionTest
 *
 * @group frontApi
 * @coversDefaultClass FrontApi\Inc\Models\Subscription
 */
class SubscriptionDataTest extends TestCase {

	const SUBSCRIPTION_ID = 1722;
	const TRIAL_ID        = 5561;

	/**
	 * Test of getInstance method.
	 *
	 * Check instance returning by correct IDs.
	 */
	public function test_getInstanceSuccess() {
		$subscription = Subscription::getInstance( self::SUBSCRIPTION_ID );
		$this->assertInstanceOf( Subscription::class, $subscription );
	}

	/**
	 * Test of getInstance method.
	 *
	 * Check returning of exception when received incorrect subscription ID.
	 */
	public function test_getInstanceException(): void {
		$this->expectException( FrontApiException::class );
		Subscription::getInstance( 0 );
	}

	/**
	 * Test of getCartSubscriptionId method.
	 *
	 * The method should return subscription ID that has been added to cart.
	 */
	public function test_getCartSubscriptionIdFromCart() {
		$ids = array( self::SUBSCRIPTION_ID, self::TRIAL_ID );

		foreach ( $ids as $id ) {
			WC()->cart->empty_cart();
			WC()->cart->add_to_cart( $id, 1 );
			$this->assertSame( $id, Subscription::getCartSubscriptionId() );
		}
	}

	/**
	 * Test of getCartSubscriptionId method.
	 *
	 * If there is no subscription in the cart, method should return NULL.
	 */
	public function test_getCartSubscriptionIdReturnEmpty() {
		WC()->cart->empty_cart();
		$this->assertNull( Subscription::getCartSubscriptionId() );
	}

	/**
	 * Test of getSubscriptionId method.
	 *
	 * If subscription with current plan is exist, method should return founded subscription ID.
	 *
	 * @param int    $id             Subscription's ID.
	 * @param string $type           Subscription's type.
	 * @param int    $servings       Subscription's portions quantity.
	 * @param int    $meals          Subscription's meals quantity.
	 *
	 * @dataProvider subscriptionPlanProvider
	 */
	public function test_getSubscriptionId( int $id, string $type, int $servings, int $meals ) {
		$subscription_id = Subscription::getSubscriptionId( $servings, $meals, $type );
		$this->assertSame( $id, $subscription_id );
	}

	/**
	 * Test of getSubscriptionId method.
	 *
	 * If subscription with current plan is not exist, method should return founded subscription ID.
	 */
	public function test_getSubscriptionIdExpectNull() {
		$subscription_id = Subscription::getSubscriptionId( 0, 100, 'trial' );
		$this->assertNull( $subscription_id );
	}

	/**
	 * Test of getSubscriptionData method.
	 *
	 * The method should return array of subscription data.
	 */
	public function test_getSubscriptionData() {
		$subscription       = Subscription::getInstance( self::SUBSCRIPTION_ID );
		$data               = $subscription->getSubscriptionData();
		$subscription_types = array(
			'subscription' => 1,
			'trial'        => 1,
		);

		$this->assertIsArray( $data );
		$this->assertArrayHasKey( 'id', $data );
		$this->assertIsInt( $data['id'] );
		$this->assertArrayHasKey( 'type', $data );
		$this->assertArrayHasKey( $data['type'], $subscription_types );
		$this->assertArrayHasKey( 'title', $data );
		$this->assertIsString( $data['title'] );
		$this->assertNotEmpty( $data['title'] );
		$this->assertArrayHasKey( 'price', $data );
		$this->assertIsNumeric( $data['price'] );
		$this->assertArrayHasKey( 'portions', $data );
		$this->assertIsInt( $data['portions'] );
		$this->assertArrayHasKey( 'days', $data );
		$this->assertIsInt( $data['days'] );
	}

	/**
	 * Test returning subscription plan by ID.
	 *
	 * @param int    $id             Subscription's ID.
	 * @param string $type           Subscription's type.
	 * @param int    $servings       Subscription's portins quantity.
	 * @param int    $meals          Subscription's meals quantity.
	 *
	 * @dataProvider subscriptionPlanProvider
	 */
	public function testGetSubscriptionPlan( int $id, string $type, int $servings, int $meals ) {
		$subscription = Subscription::getInstance( $id );
		$plan         = $subscription->getSubscriptonPlan();

		$this->assertSame( $servings, $plan['portions'] );
		$this->assertSame( $meals, $plan['days'] );
	}

	/**
	 * Test returning subscription type.
	 */
	public function testGetType() {
		$subscription = Subscription::getInstance( self::SUBSCRIPTION_ID );
		$this->assertSame( 'subscription', $subscription->getType() );

		$trial = Subscription::getInstance( self::TRIAL_ID );
		$this->assertSame( 'trial', $trial->getType() );
	}

	/**
	 * Test returning subscription ID.
	 */
	public function testGetId() {
		$subscription = Subscription::getInstance( self::SUBSCRIPTION_ID );
		$this->assertSame( self::SUBSCRIPTION_ID, $subscription->getId() );
	}

	/**
	 * Test returning subscription price.
	 */
	public function testGetPrice() {
		$product      = wc_get_product( self::SUBSCRIPTION_ID );
		$subscription = Subscription::getInstance( self::SUBSCRIPTION_ID );
		$this->assertSame( (float) $product->get_price(), (float) $subscription->getPrice() );
	}

	/**
	 * Test returning subscription day price.
	 */
	public function testGetDayPrice() {
		$product      = wc_get_product( self::SUBSCRIPTION_ID );
		$days         = get_post_meta( self::SUBSCRIPTION_ID, 'meals_quantity', true );
		$day_price    = round( $product->get_price() / $days, 2 );
		$subscription = Subscription::getInstance( self::SUBSCRIPTION_ID );
		$this->assertSame( $day_price, $subscription->getDayPrice() );
	}

	/**
	 * Test returning subscription days property.
	 */
	public function testGetDays() {
		$days         = (int) get_post_meta( self::SUBSCRIPTION_ID, 'meals_quantity', true );
		$subscription = Subscription::getInstance( self::SUBSCRIPTION_ID );
		$this->assertSame( $days, $subscription->getDays() );
	}

	/**
	 * Test returning subscription portions property.
	 */
	public function testGetPortions() {
		$portions     = (int) get_post_meta( self::SUBSCRIPTION_ID, 'serving_quantity', true );
		$subscription = Subscription::getInstance( self::SUBSCRIPTION_ID );
		$this->assertSame( $portions, $subscription->getPortions() );
	}

	/**
	 * Test returning subscription title.
	 */
	public function testGetTitle() {
		$product      = new WC_Product( self::SUBSCRIPTION_ID );
		$subscription = Subscription::getInstance( self::SUBSCRIPTION_ID );
		$this->assertSame($product->get_title(), $subscription->getTitle() );
	}

	/**
	 * Prepare meals quantity for testing.
	 *
	 * @codeCoverageIgnore
	 */
	public function subscriptionPlanProvider(): array {
		$s_servings = (int) get_post_meta( self::SUBSCRIPTION_ID, 'serving_quantity', true );
		$s_meals    = (int) get_post_meta( self::SUBSCRIPTION_ID, 'meals_quantity', true );
		$t_servings = (int) get_post_meta( self::TRIAL_ID, 'serving_quantity', true );
		$t_meals    = (int) get_post_meta( self::TRIAL_ID, 'meals_quantity', true );

		return array(
			array( self::SUBSCRIPTION_ID, 'subscription', $s_servings, $s_meals ),
			array( self::TRIAL_ID, 'trial', $t_servings, $t_meals ),
		);
	}
}