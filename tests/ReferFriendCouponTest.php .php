<?php
use PHPUnit\Framework\TestCase;
require_once __DIR__.'/../../../../wp-load.php';

class ReferFriendCouponTest extends TestCase
{
	/**
	 * @covers YPIReferFriend::user_meta_value_is_unique
	 */
	function test_user_meta_value_is_unique(){
		$meta_key = "some_key_123";
		$meta_value = "some_value_123";

		//delete old
		delete_user_meta( 30, $meta_key, $meta_value ) ;

		$this->assertTrue(YPIReferFriend::user_meta_value_is_unique($meta_key, $meta_value));

		add_user_meta( 30, $meta_key, $meta_value, true );

		$this->assertFalse(YPIReferFriend::user_meta_value_is_unique($meta_key, $meta_value));

		//Cleanup
		delete_user_meta(30, $meta_key, $meta_value );
	}

	/**
	 * @covers YPIReferFriend::create_referral_coupon_for_user
	 */
	function test_create_referral_coupon_for_user(){
		$user = get_user_by( 'id', 30 );

		delete_user_meta( 30, 'user_referral_code' );
		$coupon_code = YPIReferFriend::create_referral_coupon_for_user($user);
		$this->assertNotEmpty($coupon_code);

		$code = get_user_meta( $user->ID, 'user_referral_code' );
		$this->assertSame($coupon_code, $code[0]);
		unset($code);

		$coupon_object = new WC_Coupon($coupon_code);

		$this->assertEquals(user2_discount_amount, $coupon_object->get_amount());
		$this->assertSame(
			"Referral code for " . $user->first_name . " " . $user->last_name,
			$coupon_object->get_description()
		);
		if (class_exists('SV_WC_Coupon_Compatibility')) {
			$this->assertSame($coupon_code,SV_WC_Coupon_Compatibility::get_meta( $coupon_object, '_wc_url_coupons_unique_url', true ));
		}
		// $this->assertSame(referral_coupon_redirect_page_id,SV_WC_Coupon_Compatibility::get_meta( $coupon_object, '_wc_url_coupons_redirect_page', true ));
		$this->assertSame(1, $coupon_object->get_usage_limit_per_user());
	}
}