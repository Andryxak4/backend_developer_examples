<?php

namespace FrontApi\Tests\Inc\Models;

use FrontApi\Inc\Models\CPAdditionalComponentData;
use FrontApi\Inc\Models\CPComponentData;
use FrontApi\Inc\Models\CPExtraComponentData;
use FrontApi\Inc\Models\CPProteinComponentData;
use FrontApi\Inc\Models\CPServingComponentData;
use PHPUnit\Framework\TestCase;
use WC_CP_Component;
use WC_Product_Composite;
use WP_Query;

/**
 * Class ComponentDataParent
 *
 * @package FrontApi\Tests\Inc\Models
 *
 * @codeCoverageIgnore
 */
abstract class ComponentDataParent extends TestCase {

	const SUBSCRIPTION_ID = 1722;
	const TRIAL_ID        = 5561;

	/**
	 * Composite products IDs.
	 *
	 * @var array
	 */
	protected $composite_ids;

	/**
	 * Type of
	 *  composite product component.
	 *
	 * @var string
	 */
	protected $component_type;

	/**
	 * Classname of testing instance.
	 *
	 * @var string
	 */
	protected $testing_instance;

	/**
	 * ComponentDataParent constructor.
	 *
	 * @param null   $name TestCase param.
	 * @param array  $data TestCase param.
	 * @param string $dataName TestCase param.
	 *
	 * @codeCoverageIgnore
	 */
	public function __construct( $name = null, array $data = array(), $dataName = '' ) {

		parent::__construct( $name, $data, $dataName );

		$args = array(
			'post_type'      => 'product',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
			'tax_query'      => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'product_type',
					'field'    => 'slug',
					'terms'    => 'composite',
				),
			),
		);

		$query = new WP_Query( $args );

		if ( $query->have_posts() ) {
			foreach ( $query->posts as $post ) {
				$this->composite_ids[] = $post->ID;
			}
		} else {
			$this->composite_ids = array();
		}
	}

	/**
	 * Get composite product component.
	 *
	 * Get composite product component by ID of composite
	 * product and type of composite product component.
	 *
	 * @param int    $composite_product_id Composite product ID.
	 * @param string $type Type of composite product component.
	 *
	 * @return mixed|WC_CP_Component|null
	 *
	 * @codeCoverageIgnore
	 */
	protected function getComponent( int $composite_product_id, string $type ) {

		if ( ! in_array( $type, array( 'protein', 'servings', 'extra', 'additional-meal' ), true ) ) {
			return null;
		}

		$product = $this->getCompositeProduct( $composite_product_id );

		/**
		 * @var WC_CP_Component $component Component of composite product.
		 */
		foreach ( $product->get_components() as $component ) {
			$component_item_id = $component->get_options()[0] ?? null;

			if ( ! empty( $component_item_id ) && has_term( $type, 'product_cat', $component_item_id ) ) {
				return $component;
			}
		}

		return null;
	}

	/**
	 * Get composite product by ID.
	 *
	 * @param int $composite_product_id Composite product ID.
	 *
	 * @return WC_Product_Composite|null
	 *
	 * @codeCoverageIgnore
	 */
	protected function getCompositeProduct( int $composite_product_id ): ?WC_Product_Composite {

		if ( empty( $composite_product_id ) ) {
			return null;
		}

		return new WC_Product_Composite( $composite_product_id );
	}

	/**
	 * Get data of testing instance.
	 *
	 * @param int $composite_product_id ID of composite product.
	 *
	 * @return array
	 *
	 * @codeCoverageIgnore
	 */
	protected function getTestingInstanceData( int $composite_product_id ): array {
		$component = $this->getComponent( $composite_product_id, $this->component_type );

		/**
		 * The instance which was extended from CPComponentData class.
		 *
		 * @var CPComponentData $cp_component_data_child ;
		 */
		$cp_component_data_child = new $this->testing_instance( $component );

		return $cp_component_data_child->getData();
	}
}