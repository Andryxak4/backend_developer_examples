<?php
/**
 * Unit test for cron jobs.
 *
 * @package youplateit
 */

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
require_once __DIR__ . '/../../../../wp-load.php';
require_once __DIR__ . '/../lib/vendor/autoload.php';
use \Cron\CronExpression;

/**
 * Unit test case for auto orders.
 */
final class CronTest extends TestCase {

	/**
	 * Test that the cron calls existing functions.
	 */
	public function testCronjobsExist() {
		$cronjobs = YouPlateIt::$cronjobs;
		foreach ($cronjobs as $job) {
			$function_to_call = $job['call'];
			if (is_string($function_to_call)) {
				$this->assertTrue(function_exists($function_to_call), "Function $function_to_call does not exist.");
			} elseif (is_array($function_to_call)) {
				$this->assertTrue(
					class_exists($function_to_call[0]),
					'Class '.$function_to_call[0].' does not exist.'
				);
				$this->assertTrue(
					method_exists($function_to_call[0], $function_to_call[1]),
					'Method '.$function_to_call[1].' does not exist in class '.$function_to_call[0]
				);
			}
		}
	}

	/**
	 * Test all expressions are valid.
	 */
	public function testCronExpressions() {
		$cronjobs = YouPlateIt::$cronjobs;
		foreach ($cronjobs as $job) {
			// In case one of the expressions is faulty this will raise InvalidArgumentException
			$cron = CronExpression::factory($job['cron']);
			$this->assertNotNull($cron);
		}
	}

	/**
	 * Test the is_production functions.
	 * We are by definition running tests on a test instance.
	 * @covers is_production_environment
	 * @covers is_really_production_environment
	 */
	public function testIsProduction() {
		$this->assertFalse(is_production_environment());
		$this->assertFalse(is_really_production_environment());
	}
}
